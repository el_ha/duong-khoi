const max = 100;
fi = 'data.txt';
type tqueqe = array[0..max] of Integer;
ta = array[1..max,1..max] of Boolean;
tvisited = array[1..max] of Boolean;
var first,last,n,m,s,i: Integer;
	queqe: tqueqe;
	a,b: ta;
	visited: tvisited;
procedure readData;
	var f: text;
	u,v: integer;
	begin
		assign(f, fi);
		reset(f);
		readln(f,n,m,s);
		for i:= 1 to m do 
			begin
				readln(f,u,v);
				a[u,v] := true;
			end;
		close(f);
	end;		
procedure init;
	begin
		for i:= 1 to n do visited[i]:=false;
	end;	  
procedure add(x:Integer);
 	begin
 		inc(last);
 		queqe[last]:=x;
 	end;
procedure get(var x: Integer);
	begin
		x:=queqe[first];
		inc(first);
	end;
procedure resetQueqe;
	begin
		first:=1;
		last:=0;
	end;
procedure bfs_visit(s:Integer);
	var x,y: integer;
	begin
		resetQueqe;
		add(s);
		write(s:5);
		visited[s]:=true;
		while last >= first do
			begin
				get(x);
				for y:=1 to n do
					if a[x,y] and (not visited[y]) then
						begin
							add(y);
							visited[y]:=true;
							write(y:5);
						end;
			end;
			
	end;
procedure bfs;
	begin
		for i:=1 to n do 
			if not visited[i] then bfs_visit(i);
	end;
function lienthong:Boolean;
	begin
		init;
		lienthong:=false;
		bfs_visit(1);
		for i:=1 to n do if not visited[i] then exit;
		lienthong:=true;
	end;
function tplt : integer;
	begin
		init;
		tplt := 0;
		for i:=1 to n do 
			if not visited[i] then
				begin
					inc(tplt);
					bfs_visit(i);
				end;
	end;
procedure chuyenvi;
	var i,j: integer;
	begin
		for i:=1 to n do
			for j := 1 to n do
				b[i,j] := a[j,i];
		a:=b;
	end;
function lt_cohuong: Boolean;
	begin
		init;
		bfs_visit(1);
		lt_cohuong:=false;
		for i:=1 to n do if not visited[i] then exit;
		init;
		chuyenvi;
		bfs_visit(1);
		for i := 1 to n do if not visited[i] then exit; 
		lt_cohuong:=true;
	end;
function cau(x,y: integer): Boolean;
	var tp1:integer;
	begin
		init;
		tp1 := tplt;
		writeln(tp1);
		a[x,y]:=false;
		a[y,x]:=false;
		cau:=tp1<tplt;
		writeln(tplt);
	end;
begin
	readData;
	init;
	writeln(cau(2,4):5);
	readln;
end.