{ n la so diem m la so canh s la diem bat dau e la diem ket }
const dt = 'D:\bt\dfs\graph.txt';
var
	n,m,s,e,i: Integer;
	f: text;
	free: array[1..100] of Boolean;
	u: array[1..100,1..100] of Boolean;
	trace: array[1..100] of Integer;
procedure readData;
	var k,l: integer;
	begin
		fillchar(free,sizeof(free),true); { tat ca deu chua duoc tham}
		fillchar(u,sizeof(u),false);
		readln(f,n,m,s,e);
		for i:=1 to m do 
			begin
				readln(f,k,l);
				u[k,l]:=true;
				u[l,k]:=true;
			end;
	end;
procedure DFS(x:Integer); {tim bat dau tu vi tri x}
	begin
		write(x,', ');
		free[x]:=false;
		for i:=1 to n do 
			if free[i] and u[x,i] then
				begin
					trace[i]:=x;
					DFS(i);
				end;
	end;
procedure print;
	begin
		writeln;
		if free[e] then writeln('path from' , s,e ,'not found')
		else
			begin
				while s <> e do
					begin
						write(e,'<--');
						e:=trace[e];
					end;
				write(s);
			end;
	end;
begin
	assign(f,dt);
	reset(f);
	readData;
	DFS(s);
	print;
	close(f);
	readln
end.