const
	max = 15000;
	amax = 100;
	dt = 'd:/bt/dfs/dothingan.txt';
var
	i,j,n,s,e: Integer;
	a:array[1..amax,1..amax] of Integer;
	f:array[1..amax] of Integer;
	trace:array[1..amax] of Integer; 
procedure readData;
	var fi: text;
	begin
		assign(fi, dt);
		reset(fi);
		readln(fi,n,s,e);
		for i:=1 to n do
			begin
				for j := 1 to n do
					begin
						read(fi,a[i,j]);
						if i=j then a[i,j] := 0 
						else 
							if a[i,j] = 0 then a[i,j] := max;
					end;
				readln(fi); 
			end;
		close(fi);
	end;
procedure init;
	begin
		for i:=1 to n do 
			begin
				f[i]:=a[s,i];
				trace[i]:=s;
			end;
	end;
procedure solve;
	var stop: Boolean;
	begin
		repeat
			stop:=true;
 			for i := 1 to n do
 				for j := 1 to n do
 					if f[j] > f[i]+a[i,j] then begin
 						f[j]:=f[i]+a[i,j];
 						trace[j]:=i;
 						stop:=false;
 					end;
		until stop;
	end;
procedure print;
	var tong: Integer;
	begin
		tong:=0;
		if f[e] = max then write('khong thay')
		else
			begin
				while s<>e do
					begin
						write(e,'<--');
					 	tong:=tong+a[e,trace[e]];
					 	e:=trace[e];
					end;
				writeln(s);
			end;
		write(tong);
	end;
begin
	readData;
	init;
	solve;
	print;
end.