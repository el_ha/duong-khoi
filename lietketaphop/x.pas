const fi = 'data.txt';
var n,k: integer;
x:array[0..30] of integer;

procedure readData;
  var f:text;
  begin
    assign(f, fi); reset(f);
    readln(f,n,k);
    close(f);
  end;
procedure printResult;
  var i : integer;
  begin
    write('{');
    for i:= 1 to k - 1 do write(x[i],' ');
    writeln(x[k],'}');
  end;
procedure solve(i: integer);
  var j: integer;
  begin
    for j:= (x[i - 1] + 1) to (n - k + i) do
      begin
        x[i]:=j;
        if i = k then
          printResult
        else solve(i + 1);
      end;
  end;
begin
  readData;
  x[0]:=0;
  solve(1);
  readln;
end.
