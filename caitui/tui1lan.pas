const dt = 'd:/bt/caitui/tui.txt';
max = 1000;
var n,m,i,j: Integer;
v: array[1..max] of Integer;
w: array[0..max] of Integer;
f: array[0..max,0..max] of Integer;
procedure readData;
	var fi:text;
	begin
		assign(fi, dt);
		reset(fi);
		readln(fi,n,m);
		for i:=1 to n do
			readln(fi,w[i],v[i]);
		close(fi);
	end;
procedure init;
	begin
		fillchar(f[0],n,0);
	end;
procedure solve;
	begin
		for i:=1 to n do
			for j:=0 to m do
				begin
					f[i,j]:=f[i-1,j];
					if (j >= w[i]) and (f[i,j] < f[i-1,j-w[i]] + v[i]) 
						then f[i,j] := f[i-1,j-w[i]] + v[i];
				end;
	end;
procedure print;
	begin
		writeln(f[n,m]);
		while n<>0 do
			begin
				if f[n,m] <> f[n-1,m] then
					begin
						writeln(n);
						m:=m-w[n];
					end;
				dec(n);
			end;
	end;
begin
	readData;
	init;
	solve;
	print;
	readln;
end.