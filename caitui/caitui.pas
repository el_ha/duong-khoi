const dt = 'd:/bt/caitui/tui.txt';
max = 1000;
var n,m,i,j: Integer;
v,w,f,trace: array[1..max] of Integer;
procedure readData;
	var fi:text;
	begin
		assign(fi, dt);
		reset(fi);
		readln(fi,n,m);
		for i:=1 to n do
			readln(fi,v[i],w[i]);
		close(fi);
	end;
procedure init;
	begin
		for i:= 0 to m do
			f[i]:=0;
	end;
procedure solve;
	begin
		for i:=1 to m do
			for j:=1 to n do
				if (i >= v[j]) and (f[i]<f[i-v[j]]+w[j])
					then 
						begin
							f[i]:=f[i-v[j]]+w[j];
						end; 

	end;
procedure print;
	var best: Integer;
	begin
		best:=0;
		for i:=1 to m do
			if f[i]>best then best:=f[i];
		writeln(best);
		
	end;
begin
	readData;
	init;
	solve;
	print;
	readln;
end.
